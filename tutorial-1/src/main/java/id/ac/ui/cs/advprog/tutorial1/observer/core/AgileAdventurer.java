package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                this.guild = guild;
                guild.add(this);
        }

        @Override
        public void update() {
                if (!guild.getQuestType().equals("E"))
                        getQuests().add(guild.getQuest());
        }
}

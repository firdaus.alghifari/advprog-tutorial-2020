package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell latestSpell;
    private boolean undoUsed;

    public ContractSeal() {
        spells = new HashMap<>();
        latestSpell = new BlankSpell();
        undoUsed = false;
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        latestSpell = spells.get(spellName);
        latestSpell.cast();
        undoUsed = false;
    }

    public void undoSpell() {
        if (!undoUsed) {
            latestSpell.undo();
            undoUsed = true;
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}

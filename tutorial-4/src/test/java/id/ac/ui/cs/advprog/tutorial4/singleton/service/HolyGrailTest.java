package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    @Mock
    private HolyWish academyRepository;

    @InjectMocks
    private HolyGrail academyService;

    @Test
    public void whenMakeAWishIsCalledItShouldCallHolyWishSetWish() {
        String wish = "I want to be a flying cat";

        academyService.makeAWish(wish);
        verify(academyRepository, times(1)).setWish(wish);
    }
}

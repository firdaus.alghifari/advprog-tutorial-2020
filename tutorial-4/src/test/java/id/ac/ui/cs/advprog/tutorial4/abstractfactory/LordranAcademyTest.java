package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        lordranAcademy = new LordranAcademy();
        majesticKnight = lordranAcademy.getKnight("majestic");
        metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        syntheticKnight = lordranAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        assertEquals(majesticKnight.getName(), "Majestic");
        assertEquals(metalClusterKnight.getName(), "Metal Cluster");
        assertEquals(syntheticKnight.getName(), "Synthetic");
    }

    @Test
    public void checkKnightDescriptions() {
        assertEquals(majesticKnight.getArmor().getName(),"Shining Armor");
        assertEquals(majesticKnight.getWeapon().getName(), "Shining Buster");
        assertEquals(majesticKnight.getSkill(), null);

        assertEquals(metalClusterKnight.getArmor().getName(),"Shining Armor");
        assertEquals(metalClusterKnight.getWeapon(), null);
        assertEquals(metalClusterKnight.getSkill().getName(), "Shining Force");

        assertEquals(syntheticKnight.getArmor(),null);
        assertEquals(syntheticKnight.getWeapon().getName(), "Shining Buster");
        assertEquals(syntheticKnight.getSkill().getName(), "Shining Force");
    }
}

package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon;

public class ShiningBuster implements Weapon {

    @Override
    public String getName() {
        return "Shining Buster";
    }

    @Override
    public String getDescription() {
        return "A super shiny buster that will kill the enemy in one shot";
    }
}

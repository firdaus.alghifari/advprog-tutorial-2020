package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon;

public class ThousandJacker implements Weapon {

    @Override
    public String getName() {
        return "Thousand Jacker";
    }

    @Override
    public String getDescription() {
        return "A Jacker that slashes around 1000 times in one attack";
    }
}

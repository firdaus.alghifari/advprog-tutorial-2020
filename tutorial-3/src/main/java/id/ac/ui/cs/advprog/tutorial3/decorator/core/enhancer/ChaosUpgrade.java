package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;

    public ChaosUpgrade(Weapon weapon) {

        Random random = new Random();
        this.weapon= weapon;
        weaponValue = weapon.getWeaponValue() + random.nextInt(6) + 50;
        weaponDescription = weapon.getDescription();
        weaponName = weapon.getName();
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        return weaponValue;
    }

    @Override
    public String getDescription() {
        return weaponDescription;
    }
}

package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;

    public UniqueUpgrade(Weapon weapon){

        Random random = new Random();
        this.weapon = weapon;
        weaponValue = weapon.getWeaponValue() + random.nextInt(6) + 10;
        weaponDescription = weapon.getDescription();
        weaponName = weapon.getName();
    }

    @Override
    public String getName() {
        return weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        return weaponValue;
    }

    @Override
    public String getDescription() {
        return weaponDescription;
    }
}

package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {

    private String name;
    private String role;
    private List< Member > bawahanLst;

    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
        bawahanLst = new ArrayList< Member >();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        if (bawahanLst.size() < 3 || role.equals("Master"))
            bawahanLst.add(member);
    }

    @Override
    public void removeChildMember(Member member) {
        bawahanLst.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return bawahanLst;
    }
}

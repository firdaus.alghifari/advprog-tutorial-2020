package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;

    public RegularUpgrade(Weapon weapon) {

        Random random = new Random();
        this.weapon= weapon;
        weaponValue = weapon.getWeaponValue() + random.nextInt(5) + 1;
        weaponDescription = weapon.getDescription();
        weaponName = weapon.getName();
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        return weaponValue;
    }

    @Override
    public String getDescription() {
        return weaponDescription;
    }
}

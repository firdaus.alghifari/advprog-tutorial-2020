package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PremiumMemberTest {
    private Member member;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
        guildMaster = new PremiumMember("Eko", "Master");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals(member.getName(), "Wati");
    }

    @Test
    public void testMethodGetRole() {
        assertEquals(member.getRole(), "Gold Merchant");
    }

    @Test
    public void testMethodAddChildMember() {
        Member naruto = new OrdinaryMember("Naruto", "Ninja");
        member.addChildMember(naruto);
        assertTrue(member.getChildMembers().contains(naruto));
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member naruto = new OrdinaryMember("Naruto", "Ninja");
        member.addChildMember(naruto);
        member.removeChildMember(naruto);
        assertFalse(member.getChildMembers().contains(naruto));
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        Member naruto = new OrdinaryMember("Naruto", "Ninja");
        Member hinata = new OrdinaryMember("Hinata", "Ninja");
        Member sasuke = new OrdinaryMember("Sasuke", "Ninja");
        Member sakura = new OrdinaryMember("Sakura", "Ninja");

        member.addChildMember(naruto);
        assertTrue(member.getChildMembers().contains(naruto));
        member.addChildMember(hinata);
        assertTrue(member.getChildMembers().contains(hinata));
        member.addChildMember(sasuke);
        assertTrue(member.getChildMembers().contains(sasuke));
        member.addChildMember(sakura);
        assertFalse(member.getChildMembers().contains(sakura));
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member naruto = new OrdinaryMember("Naruto", "Ninja");
        Member hinata = new OrdinaryMember("Hinata", "Ninja");
        Member sasuke = new OrdinaryMember("Sasuke", "Ninja");
        Member sakura = new OrdinaryMember("Sakura", "Ninja");

        guildMaster.addChildMember(naruto);
        assertTrue(guildMaster.getChildMembers().contains(naruto));
        guildMaster.addChildMember(hinata);
        assertTrue(guildMaster.getChildMembers().contains(hinata));
        guildMaster.addChildMember(sasuke);
        assertTrue(guildMaster.getChildMembers().contains(sasuke));
        guildMaster.addChildMember(sakura);
        assertTrue(guildMaster.getChildMembers().contains(sakura));
    }
}

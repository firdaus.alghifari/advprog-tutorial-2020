package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals(member.getName(), "Nina");
    }

    @Test
    public void testMethodGetRole() {
        assertEquals(member.getRole(), "Merchant");
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        Member naruto = new OrdinaryMember("Naruto", "Ninja");
        member.addChildMember(naruto);
        assertFalse(member.getChildMembers().contains(naruto));
        member.removeChildMember(naruto);
        assertFalse(member.getChildMembers().contains(naruto));
    }
}

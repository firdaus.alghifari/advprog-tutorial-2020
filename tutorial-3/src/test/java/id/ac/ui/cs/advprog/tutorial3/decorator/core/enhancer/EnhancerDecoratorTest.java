package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.WeaponProducer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;



public class EnhancerDecoratorTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;
    Weapon weapon5;
    @Test
    public void testAddWeaponEnhancement(){

        Weapon gun;
        gun = WeaponProducer.WEAPON_GUN.createWeaponEnhancer();
        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(gun);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon1);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon2);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon3);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon4);

        assertTrue(weapon5.getWeaponValue() >= 101 && weapon5.getWeaponValue() <= 125);
    }

}

package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.data.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRespository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SoulServiceTest {

    @Mock
    private SoulRespository soulRespository;

    @InjectMocks
    private SoulServiceImpl soulService;

    @Test
    public void testFindAllWillCallSoulRepositoryFindAll() {
        soulService.findAll();

        verify(soulRespository, times(1)).findAll();
    }

    @Test
    public void testFindSoulWillCallSoulRepositoryFindById() {
        soulService.findSoul((long)1);

        verify(soulRespository, times(1)).findById((long)1);
    }

    @Test
    public void testEraseWillCallSoulRepositoryDeleteById() {
        soulService.erase((long)1);

        verify(soulRespository, times(1)).deleteById((long)1);
    }

    @Test
    public void testRewriteWillCallSoulRepositorySave() {
        Soul soul = new Soul(1, "a", 2, "F", "c");
        soulService.rewrite(soul);

        verify(soulRespository, times(1)).save(soul);
    }

    @Test
    public void testRegisterWillCallSoulRepositorySave() {
        Soul soul = new Soul(1, "a", 2, "F", "c");
        soulService.register(soul);

        verify(soulRespository, times(1)).save(soul);
    }

}

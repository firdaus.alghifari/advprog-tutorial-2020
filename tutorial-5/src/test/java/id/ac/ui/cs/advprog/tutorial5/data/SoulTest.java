package id.ac.ui.cs.advprog.tutorial5.data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class SoulTest {

    private Soul soul;

    @BeforeEach
    public void setUp() throws Exception {
        soul = new Soul(100, "Alghi", 20, "M", "Student");
    }

    @Test
    public void testGetId() {
        assertEquals(soul.getId(), 100);
    }

    @Test
    public void testGetName() {
        assertEquals(soul.getName(), "Alghi");
    }

    @Test
    public void testGetAge() {
        assertEquals(soul.getAge(), 20);
    }

    @Test
    public void testGetGender() {
        assertEquals(soul.getGender(), "M");
    }

    @Test
    public void testGetOccupation() {
        assertEquals(soul.getOccupation(), "Student");
    }

    @Test
    public void testSetId() {
        soul.setId(105);
        assertEquals(soul.getId(), 105);
    }

    @Test
    public void testSetName() {
        soul.setName("Kocheng");
        assertEquals(soul.getName(), "Kocheng");
    }

    @Test
    public void testSetAge() {
        soul.setAge(50);
        assertEquals(soul.getAge(), 50);
    }

    @Test
    public void testSetGender() {
        soul.setGender("F");
        assertEquals(soul.getGender(), "F");
    }

    @Test
    public void testSetOccupation() {
        soul.setOccupation("Software Engineer");
        assertEquals(soul.getOccupation(), "Software Engineer");
    }

}

package id.ac.ui.cs.advprog.tutorial5.controller;

import id.ac.ui.cs.advprog.tutorial5.data.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/soul")
public class SoulController {

    @Autowired
    private SoulService soulService;

    @GetMapping
    public ResponseEntity<List<Soul>> findAll() {
        return new ResponseEntity<>(soulService.findAll(), HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity create(@RequestBody Soul soul) {
        return new ResponseEntity<>(soulService.register(soul), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Soul> findById(@PathVariable Long id) {
        return new ResponseEntity<>(soulService.findSoul(id).get(), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul) {
        soul.setId(id);
        return new ResponseEntity<>(soulService.rewrite(soul), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        soulService.erase(id);
        return new ResponseEntity<>("Soul successfully deleted", HttpStatus.OK);
    }
}

package id.ac.ui.cs.advprog.tutorial5.repository;

import id.ac.ui.cs.advprog.tutorial5.data.Soul;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SoulRespository extends JpaRepository<Soul, Long> {

}
